package com.company;

public class Test {

    public static void main(String[] args) {
        //Construct three object
        Car car1 = new Car();
        Car car2 = new Car("Toyota","Red");
        Car car3 = new Car("Mercedes", "Siver", "Germany", 2020);

        // Display information
        /*car1.display();
        car2.display();
        car3.display();
        */

        /*// Increment car1's gear with 5.
        int increment = 5;
        for (int i  = 0; i<increment;i++){
            car1.incrementGear();
        }
        car1.display();
        */


        car1.incrementGear(5);
        car1.display();

        car1.incrementGear();
        car1.display();
    }
}
