package com.company;

public class Car {
    public double odometer;
    public String brand;
    public String color;
    int gear;
    String countryOfOrigin;
    int year;

    // Constructor 1
    public  Car(){

    }
    // Constructor 2
    public Car(String b,String c){
        odometer = 0.0;
        brand = b;
        color = c;
        gear = 0;
        countryOfOrigin = "Turkey";
        year = 2020;
    }
    // Constructor 3
    public Car (String b, String c, String country, int y){
        odometer = 0.0;
        brand = b;
        color = c;
        gear = 0;
        countryOfOrigin = country;
        year = y;
    }

    public Car(String b, int g, String c){
        odometer = 0.0;
        brand = b;
        color = c;
        if (gear < 0.0 || gear > 5.0 ){
            System.out.println("Error: Gear value must be in range [0,5]!");
        } else{
            gear = g;
        }
    }


    // Increment & Decrement Gear methods
    public void incrementGear(){
        if (gear >= 5){
            System.out.println("Error: Gear value must be in range [0,5]!");
        }else{
            gear = gear + 1;
        }
    }
    public void decrementGear(){
        if ( gear < 0.0){
            System.out.println("Error: Gear value must be in range [0,5]!");
        }else {
            gear = gear - 1;
        }
    }

    public void  incrementGear(int i){
        gear = gear + i;
    }

    public void decrementGear(int d){
        gear = gear - d;
    }

    // Drive method
    public void drive(double numberOfhour, double kmPerHour){
        odometer = odometer + (numberOfhour * kmPerHour);
    }

    // Get & Set Methods
    public double getOdometer(){
        return odometer;
    }
    public String getBrand(){
        return brand;
    }
    public String getColor(){
        return color;
    }
    public String getCountryOfOrigin(){
        return countryOfOrigin;
    }
    public int getGear(){
        return gear;
    }
    public int getYear() {
        return year;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }
    public void setGear(int gear) {
        this.gear = gear;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public void display(){
        System.out.println("This is a car that is manufactured in "+ countryOfOrigin + " in " + year + ".\n" +
        "It is a " + color + " " + brand + ".\n" + "Its odometer is at " + odometer+ ".\n"+ "Its gear is " + gear);
    }
}
